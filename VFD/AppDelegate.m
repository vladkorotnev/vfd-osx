//
//  AppDelegate.m
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "AppDelegate.h"
#import "ORSSerialPort.h"
#import "CD7220.h"
#import "VFDScreenManager.h"
#import "VFDTimeScreen.h"
#import "VFDTunesWatcher.h"
#import "VFDScreenCarousel.h"
#import "NSStringCentering.h"
#import "VFDCPUMonitor.h"
#import "VFDRAMMonitor.h"
#import "VFDOpenWeatherMap.h"
#import "VFDCoinMarketCap.h"

@interface AppDelegate ()
{
    CD7220 *vfd;
    VFDTunesWatcher *itw;
    VFDScreenCarousel *rot;
    VFDCPUMonitor *cpu;
    VFDRAMMonitor *ram;
    VFDCoinMarketCap *btc;
    VFDCoinMarketCap *otn;
    VFDCoinMarketCap *ltc;
    VFDOpenWeatherMap *weather;
}
@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Open the port for the device
    ORSSerialPort *sp = [ORSSerialPort serialPortWithPath:@"/dev/cu.serial1"];
    [sp setBaudRate:@9600];
    [sp setParity:ORSSerialPortParityNone];
    [sp setNumberOfStopBits:1];
    [sp open];
    
    // Create device driver
    vfd = [[CD7220 alloc]initWithPort:sp];
    
    
    // Set device into the slide manager
    [[VFDScreenManager sharedInstance]setDevice:vfd];
    
    // Display a greeting for 2 seconds
    VFDScreen *hello = [[VFDScreen alloc]initWithTopLine:[VFDString centeredStringWithString:@"Hello!"] bottomLine:[VFDString centeredStringWithString:@"AkRVFD Driver v1.0.1"] displayTime:2.0 modal:false];
    [[VFDScreenManager sharedInstance] pushScreen:hello];
    
    // Create time display slide
    VFDTimeScreen *jikan = [[VFDTimeScreen alloc]init];
    jikan.displayTime = 10.0;
    
    // Create the weather display slide
    weather = [VFDOpenWeatherMap displayWithAPIKey:OWM_API_KEY andCity:479561];
    
    // Create iTunes Watcher and it's auto managed slide
    itw = [[VFDTunesWatcher alloc]initWithDisplayTime:10.0 autoManaged:true];
    
    // Create a CPU watcher
    cpu = [[VFDCPUMonitor alloc]init];
    cpu.displayTime = 5.0;
    
    // Create a RAM watcher
    ram = [[VFDRAMMonitor alloc]init];
    ram.displayTime = 5.0;
    
    // Create a Bitcoin ticker
    btc = [[VFDCoinMarketCap alloc]initWithIdentifier:@"bitcoin"];
    
    // Create an OTN ticker
    otn = [[VFDCoinMarketCap alloc]initWithIdentifier:@"open-trading-network"];
    otn.investedUSD = 2726;
    otn.balance = 87.8324883861;
    
    // Create a Litecoin ticker
    ltc = [[VFDCoinMarketCap alloc]initWithIdentifier:@"litecoin"];
    ltc.investedUSD = 60;
    ltc.balance = 0.238866;
    
    // Create a slide show rotator
    rot = [[VFDScreenCarousel alloc]initWithScreens:@[jikan, weather, ram, cpu, itw.screen, btc, ltc, otn] andSwitchTiming:5.0];
    [[VFDScreenManager sharedInstance] slipScreenBelowCurrent:rot];
}

- (BOOL)applicationShouldHandleReopen:(NSApplication *)theApplication hasVisibleWindows:(BOOL)flag
{
    // TODO: open pref pane
    return NO;
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
    [vfd clear];
    [vfd reset];
}

@end
