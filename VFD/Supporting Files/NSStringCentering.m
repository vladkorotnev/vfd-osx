//
//  NSStringCentering.m
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "NSStringCentering.h"

#define NSDOSRussianStringEncoding 0x8000041B
@implementation NSString (NSStringCentering)
- (NSUInteger) leftPaddingForRightAlignStringInWidth: (NSUInteger) width {
    if (self.length > width) {
        return 0;
    }
    return (width - self.length);
}
- (NSUInteger) leftPaddingForCenteringStringInWidth: (NSUInteger) width {
    if (self.length > width) {
        return 0;
    }
    return (width - self.length)/2;
}
- (NSString*) stringCenteredInWidth:(NSUInteger) width {
    if(self.length >= width) return self;
    NSUInteger add = [self leftPaddingForCenteringStringInWidth:width];
    NSString *pad = [@"" stringByPaddingToLength:add withString:@" " startingAtIndex:0];
    return [NSString stringWithFormat:@"%@%@",pad,self];
}
- (NSString*) stringRightAlignedInWidth:(NSUInteger) width {
    if(self.length >= width) {
        return [self substringFromIndex:(self.length-width)];
    }
    NSUInteger add = [self leftPaddingForRightAlignStringInWidth:width];
    NSString *pad = [@"" stringByPaddingToLength:add withString:@" " startingAtIndex:0];
    return [NSString stringWithFormat:@"%@%@",pad,self];
}
- (NSString*) sanitizeForVFD {
    
    return [[NSString alloc]initWithData:[self dataUsingEncoding:NSDOSRussianStringEncoding allowLossyConversion:TRUE] encoding:NSDOSRussianStringEncoding];
}
@end