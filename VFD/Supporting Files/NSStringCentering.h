//
//  NSStringCentering.h
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>

/** 
 @category NSStringCentering
 @brief Contains the methods for centering NSStrings on VFD devices
 */
@interface NSString (NSStringCentering)
/**
 * @method stringCenteredInWidth
 * @brief Returns a string padded to be centered in a specific width
 * @param width The width (in characters) to center in
 * @see stringRightAlignedInWidth
 */
- (NSString*) stringCenteredInWidth:(NSUInteger) width;
/**
 * @method leftPaddingForCenteringStringInWidth
 * @brief Returns the amount of spaces needed at the left to center the string in the specified width
 * @param width The width (in characters) to center in
 * @see leftPaddingForRightAlignStringInWidth
 */
- (NSUInteger) leftPaddingForCenteringStringInWidth: (NSUInteger) width;
/**
 * @method stringRightAlignedInWidth
 * @brief Returns a string padded to be right aligned in a specific width
 * @param width The width (in characters) to right-align in
 * @see  stringCenteredInWidth
 */
- (NSString*) stringRightAlignedInWidth:(NSUInteger) width;
/**
 * @method leftPaddingForRightAlignStringInWidth
 * @brief Returns the amount of spaces needed at the left to right-align the string in the specified width
 * @param width The width (in characters) to right-align in
 * @see leftPaddingForCenteringStringInWidth
 */
- (NSUInteger) leftPaddingForRightAlignStringInWidth: (NSUInteger) width;
/**
 * @method sanitizeForVFD
 * @brief Returns a string prepared for transmission over the VFD driver
 */
- (NSString*) sanitizeForVFD;
@end