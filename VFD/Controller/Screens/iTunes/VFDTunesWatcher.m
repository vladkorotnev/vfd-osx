//
//  VFDTunesWatcher.m
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDTunesWatcher.h"

#import "NSStringCentering.h"
@implementation VFDTunesWatcher {
    VFDScreen * iTunesScreen;
}

- (VFDTunesWatcher*) initWithDisplayTime:(NSTimeInterval)displayTime autoManaged:(bool)autoManaged modalMode:(bool)modal {
    self = [super init];
    if (self) {
        iTunesScreen = [[VFDScreen alloc]init];
        
        iTunesScreen.isModal = modal;
        self.keepDisplayingWhilePlaying = modal;
        iTunesScreen.displayTime = displayTime;
        self.displayTime = displayTime;
        self.autoManageDisplay = autoManaged;
        iTunesScreen.isInactive = true;
        
        NSDistributedNotificationCenter *dnc = [NSDistributedNotificationCenter defaultCenter];
        
        [dnc addObserver:self
                selector:@selector(updateTrackInfoFromITunes:)
                    name:@"com.apple.iTunes.playerInfo"
                  object:nil];
    }
    return self;
}
- (VFDTunesWatcher*) init {
    return [self initWithDisplayTime:5.0 autoManaged:true modalMode:false];
}

- (VFDTunesWatcher*) initWithModalMode {
    return [self initWithDisplayTime:5.0 autoManaged:true modalMode:true];
}
- (VFDTunesWatcher*) initWithDisplayTime:(NSTimeInterval)displayTime autoManaged:(bool)autoManaged{
    return [self initWithDisplayTime:displayTime autoManaged:autoManaged modalMode:false];
}


- (void)updateTrackInfoFromITunes:(NSNotification *)notification {
    
    if ([notification.userInfo[@"Player State"] isEqualToString:@"Playing"]) {
        
        NSMutableString *s = [NSMutableString new];
        
        
        if (notification.userInfo[@"Artist"]) {
            [s appendString:[notification.userInfo[@"Artist"] sanitizeForVFD]];
        }
      /*  if (notification.userInfo[@"Track Number"]) {
            [s appendString:@" | #"];
            [s appendFormat:@"%@",notification.userInfo[@"Track Number"]];
            if (notification.userInfo[@"Track Count"]) {
                [s appendFormat:@"/%@",notification.userInfo[@"Track Count"]];
            }
        } */
        
        VFDString *bottom = [VFDString centeredStringWithString:s];
        VFDString *top;
        if (notification.userInfo[@"Name"]) {
            NSString *ss = [notification.userInfo[@"Name"] sanitizeForVFD];
            top = [VFDString centeredStringWithString:ss];
        } else  top = [VFDString centeredStringWithString:@"(No Title)"];
        
        NSLog(@"notification payload: %@", notification.userInfo);
        
        iTunesScreen.topLine = top;
        iTunesScreen.bottomLine = bottom;
        iTunesScreen.isModal = self.keepDisplayingWhilePlaying;
        iTunesScreen.displayTime = self.displayTime;
        iTunesScreen.isInactive = false;
        if(self.autoManageDisplay) [[VFDScreenManager sharedInstance]pushScreen:iTunesScreen];
    } else {
        // iTunes not playing
        iTunesScreen.isInactive= true;
       if(self.autoManageDisplay) [[VFDScreenManager sharedInstance]pullScreen:iTunesScreen];
      
    }
}
- (void) dealloc {
    NSDistributedNotificationCenter *dnc = [NSDistributedNotificationCenter defaultCenter];
    [dnc removeObserver:self name:@"com.apple.iTunes.playerInfo" object:nil];
}
- (VFDScreen *) screen {
    return iTunesScreen;
}
@end
