//
//  VFDTunesWatcher.h
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VFDScreenManager.h"

/**
 @class VFDTunesWatcher
 @brief An utility class to watch for iTunes Track Notifications and display them when necessary or on a generated VFDScreen
 */
@interface VFDTunesWatcher : NSObject
/**
 * @method init
 * @brief Initializes the class with default parameters
 * @see initWithModalMode
 * @see initWithDisplayTime:autoManaged
 */
- (VFDTunesWatcher*) init;
/**
 * @method initWithModalMode
 * @brief Initialize the class, asking it to create a modal screen (i.e. for use as the only display on the system, or in a container)
 * @see screen
 * @see initWithDisplayTime:autoManaged
 */
- (VFDTunesWatcher*) initWithModalMode;
/**
 * @method initWithDisplayTime:autoManaged
 * @brief Initialize the class with the specified screen display timeout, optionally forcing to automatically display the screen when an iTunes notification arrives
 * @param displayTime The timeout to display the notification screen
 * @see displayTime
 * @param autoManaged Whether the screen should automatically be displayed when an iTunes notification is received
 * @see autoManageDisplay
 */
- (VFDTunesWatcher*) initWithDisplayTime:(NSTimeInterval)displayTime autoManaged:(bool)autoManaged;

/** 
 * @property keepDisplayingWhilePlaying
 * @brief (Auto-managed mode) Whether the notification screen should be kept on screen, until iTunes is stopped
 */
@property (nonatomic) bool keepDisplayingWhilePlaying;
/**
 * @property displayTime
 * @brief The time to display the notification screen for
 */
@property (nonatomic) NSTimeInterval displayTime;
/**
 * @property autoManageDisplay
 * @brief Whether the notification screen should be displayed automatically when an iTunes Notification is received
 */
@property (nonatomic) bool autoManageDisplay;
/**
 * @method screen
 * @brief Returns the iTunes Information auto-updated screen
 */
- (VFDScreen *) screen;
@end
