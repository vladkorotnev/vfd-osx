//
//  VFDShowCommand.h
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 @class VFDShowCommand
 @brief The 'display' command AppleScript handler
 @discussion   A very simple command to show data on display via AppleScript
 */
@interface VFDShowCommand : NSScriptCommand

@end
