//
//  VFDShowCommand.m
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDShowCommand.h"
#import "VFDScreenManager.h"
@implementation VFDShowCommand

-(id)performDefaultImplementation {
    
    // get the arguments
    NSDictionary *args = [self evaluatedArguments];

    if(args.count >= 3) {
        NSString *l1 = [args objectForKey:@"first"];
         NSString *l2 = [args objectForKey:@"second"];
         NSUInteger dur = [[args objectForKey:@"duration"]integerValue];
        VFDScreen *scr = [[VFDScreen alloc]initWithTopLine:[VFDString centeredStringWithString:l1] bottomLine:[VFDString centeredStringWithString:l2] displayTime:dur modal:false];
        scr.shouldClearPriorToDrawing = true;
        [[VFDScreenManager sharedInstance] pushScreen:scr];
    } else {
        // raise error
        [self setScriptErrorNumber:-50];
        [self setScriptErrorString:@"Parameter Error: A Parameter is expected for the verb 'display' ."];
    }

    return nil;
}

@end
