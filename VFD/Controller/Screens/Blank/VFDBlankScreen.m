//
//  VFDBlankScreen.m
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDBlankScreen.h"

@implementation VFDBlankScreen
- (VFDString*) topLine {
    return [VFDString simpleStringWithString:@""];
}
- (VFDString*) bottomLine {
    return [VFDString simpleStringWithString:@""];
}
@end
