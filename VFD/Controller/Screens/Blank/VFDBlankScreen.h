//
//  VFDBlankScreen.h
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDScreen.h"

/**
 @class VFDBlankScreen
 @brief An empty VFD screen stub
 */
@interface VFDBlankScreen : VFDScreen

@end
