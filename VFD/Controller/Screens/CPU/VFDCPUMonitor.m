//
//  VFDCPUMonitor.m
//  VFD
//
//  Created by Akasaka Ryuunosuke on 19/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDCPUMonitor.h"
#include <sys/sysctl.h>
#include <sys/types.h>
#include <mach/mach.h>
#include <mach/processor_info.h>
#include <mach/mach_host.h>

@implementation VFDCPUMonitor {
    processor_info_array_t cpuInfo, prevCpuInfo;
    mach_msg_type_number_t numCpuInfo, numPrevCpuInfo;
    unsigned numCPUs;
    NSTimer *updateTimer;
    NSLock *CPUUsageLock;
    NSMutableArray *usages;
}

- (VFDCPUMonitor *) init {
    self = [super init];
    if (self) {
        int mib[2U] = { CTL_HW, HW_NCPU };
        size_t sizeOfNumCPUs = sizeof(numCPUs);
        int status = sysctl(mib, 2U, &numCPUs, &sizeOfNumCPUs, NULL, 0U);
        if(status)
            numCPUs = 1;
        
        usages = [NSMutableArray arrayWithCapacity:numCPUs];
        for(unsigned i=0U; i<numCPUs;i++)
            usages[i] = @0;
        
        CPUUsageLock = [[NSLock alloc] init];
        
        self.topLine = [VFDString centeredStringWithString:@"CPU Usage"];
    }
    return self;
}

- (NSTimeInterval) refreshInterval {
    return 1.5;
}

- (bool) isModal {
    return true;
}

- (bool) hasUpdates {
    return true;
}


- (void) drawOnVFD:(NSObject<VFDDevice> *)device {
    [self recalculateCPU];
    [super drawOnVFD:device];
}

- (void) updateOnVFD:(NSObject<VFDDevice> *)device {
    [self recalculateCPU];
    [device setBottomLine:self.bottomLine];
}

- (void) recalculateCPU {
    natural_t numCPUsU = 0U;
    kern_return_t err = host_processor_info(mach_host_self(), PROCESSOR_CPU_LOAD_INFO, &numCPUsU, &cpuInfo, &numCpuInfo);
    if(err == KERN_SUCCESS) {
        [CPUUsageLock lock];
        
        for(unsigned i = 0U; i < numCPUs; ++i) {
            float inUse, total;
            if(prevCpuInfo) {
                inUse = (
                         (cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_USER]   - prevCpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_USER])
                         + (cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_SYSTEM] - prevCpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_SYSTEM])
                         + (cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_NICE]   - prevCpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_NICE])
                         );
                total = inUse + (cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_IDLE] - prevCpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_IDLE]);
            } else {
                inUse = cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_USER] + cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_SYSTEM] + cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_NICE];
                total = inUse + cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_IDLE];
            }
            
            usages[i] = [NSNumber numberWithFloat:(inUse / total)];
        }
        [CPUUsageLock unlock];
        
        if(prevCpuInfo) {
            size_t prevCpuInfoSize = sizeof(integer_t) * numPrevCpuInfo;
            vm_deallocate(mach_task_self(), (vm_address_t)prevCpuInfo, prevCpuInfoSize);
        }
        
        prevCpuInfo = cpuInfo;
        numPrevCpuInfo = numCpuInfo;
        
        cpuInfo = NULL;
        numCpuInfo = 0U;
    } else {
        self.bottomLine = [VFDString centeredStringWithString:@"Error."];
        return;
    }
    
    NSMutableString *cpu = [NSMutableString new];
    for (unsigned i=0U; i<numCPUs;i++) {
        [cpu appendFormat:@"%.0f%%", [((NSNumber*) usages[i]) floatValue]*100];
        if(i < numCPUs-1) [cpu appendFormat:@", "];
    }
    self.bottomLine = [VFDString centeredStringWithString:cpu];
}

@end
