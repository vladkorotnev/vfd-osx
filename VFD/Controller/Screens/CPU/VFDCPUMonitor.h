//
//  VFDCPUMonitor.h
//  VFD
//
//  Created by Akasaka Ryuunosuke on 19/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDScreen.h"
/**
 @class VFDCPUMonitor
 @brief A VFD Screen showing the current CPU load per cores
 */
@interface VFDCPUMonitor : VFDScreen
- (VFDCPUMonitor *) init;
@end
