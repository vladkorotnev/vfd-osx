//
//  VFDRAMMonitor.m
//  VFD
//
//  Created by Akasaka Ryuunosuke on 20/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDRAMMonitor.h"
#import <sys/sysctl.h>
#import <mach/host_info.h>
#import <mach/mach_host.h>
#import <mach/task_info.h>
#import <mach/task.h>

@implementation VFDRAMMonitor
{
    int mib[6];
    int pagesize;
    size_t length;
    vm_statistics_data_t vmstat;
    mach_msg_type_number_t count;
}

- (NSTimeInterval) refreshInterval {
    return 3.0;
}

- (bool) isModal {
    return true;
}

- (bool) hasUpdates {
    return true;
}



- (void) drawOnVFD:(NSObject<VFDDevice> *)device {
    self.topLine = [VFDString centeredStringWithString:@"Free, Used RAM:"];
    self.bottomLine = [VFDString centeredStringWithString:@"Querying..."];
    [self recalculateRAM];
    [super drawOnVFD:device];
}

- (void) updateOnVFD:(NSObject<VFDDevice> *)device {
    [self recalculateRAM];
    [device setBottomLine:self.bottomLine];
}

- (void) recalculateRAM {
    mib[0] = CTL_HW;
    mib[1] = HW_PAGESIZE;
    length = sizeof (pagesize);
    if (sysctl (mib, 2, &pagesize, &length, NULL, 0) < 0)
    {
        self.bottomLine = [VFDString centeredStringWithString:@"Error."]; return;
    }
    count = HOST_VM_INFO_COUNT;
    
    if (host_statistics (mach_host_self (), HOST_VM_INFO, (host_info_t) &vmstat, &count) != KERN_SUCCESS)
    {
       self.bottomLine = [VFDString centeredStringWithString:@"Error."]; return;
    }
    
    double total = vmstat.wire_count + vmstat.active_count + vmstat.inactive_count + vmstat.free_count;
    
    task_basic_info_64_data_t info;
    unsigned size = sizeof (info);
    task_info (mach_task_self (), TASK_BASIC_INFO_64, (task_info_t) &info, &size);
    
    float unit = 1024 * 1024 * 1024;
    NSString *usages = [NSString stringWithFormat: @"% 3.1f GB\n% 3.1f GB\n", ((float)vmstat.free_count *(float)pagesize)/ unit,  ((float)(total - vmstat.free_count) *(float)pagesize)/ unit];
    
    self.bottomLine.content = usages;
}

@end
