//
//  VFDRAMMonitor.h
//  VFD
//
//  Created by Akasaka Ryuunosuke on 20/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDScreen.h"
/**
 @class VFDRAMMonitor
 @brief A VFD Screen showing the current free and used RAM amount
 */
@interface VFDRAMMonitor : VFDScreen

@end
