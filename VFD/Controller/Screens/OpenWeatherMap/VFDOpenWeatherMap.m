//
//  VFDOpenWeatherMap.m
//  VFD
//
//  Created by Akasaka Ryuunosuke on 13/01/18.
//  Copyright (c) 2018 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDOpenWeatherMap.h"

@implementation VFDOpenWeatherMap {
    NSDictionary * lastResult;
    NSDate * lastResultAcquired;
}
@synthesize APIKey=_APIKey, city=_city;

#define OWMAPIEndpoint @"http://api.openweathermap.org/data/2.5/weather?units=metric&id=%lu&APPID=%@"

+ (VFDOpenWeatherMap *) displayWithAPIKey: (OWMAPIKey) APIKey andCity:(OWMCityID) city {
    return [[VFDOpenWeatherMap alloc]initWithAPIKey:APIKey andCity:city];
}

- (VFDOpenWeatherMap *) initWithAPIKey: (OWMAPIKey) APIKey {
    self = [super init];
    if (self) {
        _APIKey = APIKey;
    }
    return self;
}
- (VFDOpenWeatherMap *) initWithAPIKey: (OWMAPIKey) APIKey andCity: (OWMCityID) city {
    self = [self initWithAPIKey:APIKey];
    if (self) {
        _city = city;
    }
    return self;
}

#pragma mark - Display 

- (NSTimeInterval) displayTime {
    return 4.0;
}
- (NSTimeInterval) overrideCarouselDisplayTiming {
    return [self displayTime];
}

- (bool) hasUpdates {
    return false;
}

- (void) drawOnVFD:(NSObject<VFDDevice> *)device {
    [device clear];
    if (!lastResultAcquired || [[NSDate date] timeIntervalSinceDate:lastResultAcquired] > 10.0 * 60.0 /* 10 minutes */ ) {
        [device setTopLine:@"Loading weather..." aligned:VFDLineCentered];
        lastResult = nil;
        
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:OWMAPIEndpoint, _city, _APIKey]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5.0];
        
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue new] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if(!connectionError) {
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                lastResult = json;
            }
            dispatch_semaphore_signal(sema);
        }];
        
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        
        if (lastResult) {
            lastResultAcquired = [NSDate date];
        }
    }
    
    if (!lastResult || !lastResult[@"cod"] || [lastResult[@"cod"] integerValue] != 200) {
        [device setTopLine:@"Weather" aligned:VFDLineCentered];
        [device setTopLine:@"Loading failed" aligned:VFDLineCentered];
    } else {
        [device setTopLine:lastResult[@"weather"][0][@"description"] aligned:VFDLineNone];
        NSString * weatherReport = [NSString stringWithFormat:@"%@ C, %@ %%, %@ m/s", lastResult[@"main"][@"temp"], lastResult[@"main"][@"humidity"], lastResult[@"wind"][@"speed"]];
        [device setBottomLine:weatherReport aligned:VFDLineRight];
    }
    
    
}



@end
