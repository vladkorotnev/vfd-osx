//
//  VFDOpenWeatherMap.h
//  VFD
//
//  Created by Akasaka Ryuunosuke on 13/01/18.
//  Copyright (c) 2018 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDScreen.h"

/**
 @typedef OWMCityID
 @brief An OpenWeatherMap city identifier
 */
typedef NSUInteger OWMCityID;
/**
 @typedef OWMAPIKey
 @brief An OpenWeatherMap API key
 */
typedef NSString* OWMAPIKey;

@interface VFDOpenWeatherMap : VFDScreen

+ (VFDOpenWeatherMap *) displayWithAPIKey: (OWMAPIKey) APIKey andCity:(OWMCityID) city;

- (VFDOpenWeatherMap *) initWithAPIKey: (OWMAPIKey) APIKey;
- (VFDOpenWeatherMap *) initWithAPIKey: (OWMAPIKey) APIKey andCity: (OWMCityID) city;


@property (nonatomic, strong, readonly) OWMAPIKey APIKey;
@property (nonatomic) OWMCityID city;

@end
