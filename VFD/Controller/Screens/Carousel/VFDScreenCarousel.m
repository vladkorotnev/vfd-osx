//
//  VFDScreenCarousel.m
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDScreenCarousel.h"
#import "VFDScreenManager.h"
@implementation VFDScreenCarousel
{
    NSMutableArray *carousel;
    NSUInteger currentIndex;
    bool hasJustChanged;
}

- (VFDScreenCarousel *) initWithScreens: (NSArray *)screens andSwitchTiming:(NSTimeInterval)switchTiming {
    self = [super init];
    if (self) {
        
        self.switchTiming = switchTiming;
        
        if(screens) {
            carousel = [NSMutableArray arrayWithArray:screens];
            [self startRotation];
        }
        else carousel = [NSMutableArray new];
    }
    return self;
}

- (VFDScreenCarousel *) initWithScreens: (NSArray *)screens {
    return [self initWithScreens:screens andSwitchTiming:0.0];
}
- (VFDScreenCarousel *) init {
    return [self initWithScreens:nil];
}

- (void) drawOnVFD: (NSObject<VFDDevice>*)device {
    [[self currentScreen]drawOnVFD:device];
}

- (void) updateOnVFD:(NSObject<VFDDevice> *)device {
    [[self currentScreen]updateOnVFD:device];
}

- (void) startRotation {
   if([self currentScreen]) [self performSelector:@selector(_displayTimeExpired) withObject:nil afterDelay:[self _displayDelay]];
}

- (NSTimeInterval) _displayDelay {
    if ([[self currentScreen] respondsToSelector:@selector(overrideCarouselDisplayTiming)]) {
        return [[self currentScreen] overrideCarouselDisplayTiming];
    }
    return (self.switchTiming == 0 ? [self currentScreen].displayTime : self.switchTiming);
}


- (void) _displayTimeExpired {
    NSUInteger oldIndex = currentIndex;
    do {
        currentIndex++;
        if (currentIndex >= carousel.count) {
            currentIndex = 0;
        } else if (currentIndex == oldIndex && [self currentScreen].isInactive) {
            // looped and no active screen found!
            NSException *shit =  [NSException exceptionWithName:@"VFDNoActiveScreenException" reason:@"There was no active screen in the carousel to be the next one." userInfo:nil];
            [shit raise];
        }
    } while ([self currentScreen].isInactive);
    if(currentIndex != oldIndex) {
        // in case only one active screen, no redraw
        hasJustChanged = true;
        if ([[VFDScreenManager sharedInstance]isCurrentScreen:self]) {
            [[VFDScreenManager sharedInstance] redraw];
        }
    }
    
    [self performSelector:@selector(_displayTimeExpired) withObject:nil afterDelay:[self _displayDelay]];
}

- (VFDScreen <VFDScreenForCarousel> *) currentScreen {
    if(carousel.count == 0) return nil;
    if (currentIndex >= carousel.count) {
        currentIndex = 0;
    }
    return carousel[currentIndex];
}

- (NSTimeInterval) refreshInterval {
    return [self currentScreen].refreshInterval;
}

- (VFDString *) topLine {
    return [self currentScreen].topLine;
}

- (VFDString *) bottomLine {
    return [self currentScreen].bottomLine;
}

- (bool) hasUpdates {
    if (hasJustChanged == true) {
        hasJustChanged = false;
        return false;
    }
    return [self currentScreen].hasUpdates;
}

- (bool) isModal {
    return true; // pseudo screen for rotating screens is supposed to be root eh?
}
- (NSMutableArray *) screens {
    return carousel;
}

@end
