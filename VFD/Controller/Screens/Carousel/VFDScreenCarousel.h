//
//  VFDScreenCarousel.h
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDScreen.h"
/**
 @class VFDScreenCarousel
 @brief A VFD Screen Container displaying it's subscreens sequentially by the specified timing
 @seealso VFDScreenForCarousel
 */
@interface VFDScreenCarousel : VFDScreen
/**
 * @method initWithScreens:andSwitchTiming
 * @brief Initializes the carousel with specified subscreens and time to switch them out
 * @param screens An array of screens to cycle through
 * @param switchTiming The interval to switch between screens
 */
- (VFDScreenCarousel *) initWithScreens: (NSArray *)screens andSwitchTiming:(NSTimeInterval)switchTiming;
/**
 * @method initWithScreens
 * @brief Initializes the carousel with specified subscreens, each will be displayed for it's specified displaytime
 * @param screens An array of screens to cycle through
 * @see switchTiming
 * @see initWithScreens:andSwitchTiming
 */
- (VFDScreenCarousel *) initWithScreens: (NSArray *)screens;
/**
 * @method initWithScreens
 * @brief Initializes an empty carousel
 */
- (VFDScreenCarousel *) init;
/**
 * @method screens
 * @brief Returns the pointer to the mutable array of carousel's screens
 */
- (NSMutableArray *) screens;
/**
 * @property switchTiming
 * @brief The time to display each subscreen for
 * @remark Use 0 to switch by the DisplayTime of each subscreen
 */
@property (nonatomic) NSTimeInterval switchTiming; // 0 for use DisplayTime of inner screens
/**
 * @method startRotation
 * @brief Begins rotation of the subscreens
 */
- (void) startRotation;
@end

/**
 @protocol VFDScreenForCarousel
 @brief Contains optional methods to enhance integration of VFDScreens into VFDScreenCarousel
 @seealso VFDScreenCarousel
 */
@protocol VFDScreenForCarousel
@optional
- (NSTimeInterval) overrideCarouselDisplayTiming;
@end