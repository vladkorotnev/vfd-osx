//
//  VFDTimeScreen.m
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDTimeScreen.h"
#import "NSStringCentering.h"
@implementation VFDTimeScreen {
    NSDate *lastUpdateDate;
    bool dots;
}
- (NSTimeInterval) refreshInterval {
    return 1.0;
}

- (bool) isModal {
    return true;
}

- (bool) hasUpdates {
    return true;
}

- (void) drawOnVFD:(NSObject<VFDDevice> *)device {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSDate *currentDate = [NSDate date];
    lastUpdateDate = currentDate;
    
    [formatter setDateFormat:@"EEEE, MMMM dd"];
    NSString *s = [formatter stringFromDate:currentDate];
    /* go over different formats if the line won't fit */
    if (s.length > [device width]) {
        [formatter setDateFormat:@"EEE, MMMM dd"];
        s = [formatter stringFromDate:currentDate];
    }
    if (s.length > [device width]) {
        [formatter setDateFormat:@"MMMM dd"];
        s = [formatter stringFromDate:currentDate];
    }
    
    self.topLine = [VFDString centeredStringWithString:s];
    
    dots = !dots;
    [formatter setDateFormat: dots ? @"HH:mm:ss"  : @"HH mm ss"];
    self.bottomLine = [VFDString centeredStringWithString:[formatter stringFromDate:currentDate]];
  
    
    [device clear];
    [device overwrite];
    [device setCursorRow:1 Col:[self.topLine leftOffsetForDevice:device]+1];
    [device sendString:self.topLine.content];
    [device setCursorRow:2 Col:[self.bottomLine leftOffsetForDevice:device]+1];
    [device sendString:self.bottomLine.content];
}

- (void) updateOnVFD:(NSObject<VFDDevice> *)device {
    // less blinky version of draw:
   
    NSDate *currentDate = [NSDate date];
    
    NSUInteger dayNow = [[NSCalendar currentCalendar] ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:currentDate];
    NSUInteger dayThen = [[NSCalendar currentCalendar] ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:lastUpdateDate];
    // redraw top if we are at past midnight
    if (dayNow != dayThen) {
        [self drawOnVFD:device];
    } else {
         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        dots = !dots;
        [formatter setDateFormat: dots ? @"HH:mm:ss"  : @"HH mm ss"];
        self.bottomLine = [VFDString centeredStringWithString:[formatter stringFromDate:currentDate]];
        [device setCursorRow:2 Col:[self.bottomLine leftOffsetForDevice:device]+1];
        [device sendString:self.bottomLine.content];
        
        lastUpdateDate = currentDate;
    }
}

@end
