//
//  VFDTimeScreen.h
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDScreen.h"
/**
 @class VFDTimeScreen
 @brief A VFD Screen to display current time and date
 */
@interface VFDTimeScreen : VFDScreen

// TODO: Add format properties!

@end
