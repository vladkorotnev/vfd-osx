//
//  VFDCoinMarketCap.h
//  VFD
//
//  Created by Akasaka Ryuunosuke on 09/12/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDScreen.h"
#import "CMCAPI.h"
#import "VFDScreenCarousel.h"

/** 
 @class VFDCoinMarketCap
 @brief Displays a ticker for a CoinMarketCap crypto currency symbol
 */
@interface VFDCoinMarketCap : VFDScreen <VFDScreenForCarousel>
/**
 @method initWithIdentifier:
 @brief Initializes the ticker display for the specified identifier
 @seealso CMCTicker
 */
- (VFDCoinMarketCap *) initWithIdentifier: (CMCIdentifier *) identifier;
/**
 @property identifier
 @brief The crypto symbol, in CMC API standards, to display the ticker for
 */
@property (nonatomic, strong) CMCIdentifier * identifier;
/**
 @property investedUSD
 @brief When both investedUSD and balance properties are not zero, makes the ticker display and calculate the current savings equivalent in USD and the gross value from the invested amount
 @see balance
 */
@property (nonatomic) NSUInteger investedUSD;
/**
 @property balance
 @brief When both investedUSD and balance properties are not zero, makes the ticker display and calculate the current savings equivalent in USD and the gross value from the invested amount
 @see investedUSD
 */
@property (nonatomic) CGFloat balance;

@end
