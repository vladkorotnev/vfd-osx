//
//  CMCAPI.m
//  VFD
//
//  Created by Akasaka Ryuunosuke on 09/12/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "CMCAPI.h"

@implementation CMCAPI
+ (CMCAPI*) sharedInstance {
    static CMCAPI * api = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        api = [[CMCAPI alloc]init];
    });
    return api;
}

#define CMC_ROOT @"https://api.coinmarketcap.com/v1/ticker/%@/"
- (CMCTicker*) tickerForIdentifier: (CMCIdentifier *)identifier {
    __block CMCTicker * ticker;
    NSURLRequest * request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:CMC_ROOT, identifier]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5.0];
    
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue new] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if(!connectionError) {
            NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSDictionary *tickData = json[0];
            if(tickData) {
                ticker = [CMCTicker tickerFromJSONObject:tickData];
            }
        }
        dispatch_semaphore_signal(sema);
    }];
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);

    return ticker;
}
@end
