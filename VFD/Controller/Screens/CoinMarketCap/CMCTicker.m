//
//  CMCTicker.m
//  VFD
//
//  Created by Akasaka Ryuunosuke on 09/12/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "CMCTicker.h"
#import "CMCAPI.h"
@implementation CMCTicker {
    NSDictionary *internal;
}
+ (CMCTicker*) tickerFromJSONObject:(NSDictionary*) object {
    CMCTicker *ticker = [[CMCTicker alloc] initWithDictionary:object];
    return ticker;
}
+ (CMCTicker*) tickerForIdentifier: (NSString *)identifier {
    return [[CMCAPI sharedInstance] tickerForIdentifier:identifier];
}
- (CMCTicker *) initWithDictionary: (NSDictionary*) object {
    self = [super init];
    if (self) {
        internal = object;
    }
    return self;
}




- (NSString*) identifier {
    return internal[@"id"];
}
- (NSString*) name {
    return internal[@"name"];
}
- (NSString*) description {
    return [NSString stringWithFormat:@"<CMCTicker result for %@ (%@, %@)>", self.identifier, self.symbol, self.name];
}
- (NSString*) symbol {
    return internal[@"symbol"];
}

- (NSUInteger) rank {
    return [[internal objectForKey:@"rank"] integerValue];
}

- (double) _castDouble:(NSString*)key {
    return [[internal objectForKey:key] doubleValue];
}
- (double) priceUSD {
    return [self _castDouble:@"price_usd"];
}
- (double) priceBTC{
    return [self _castDouble:@"price_btc"];
}
- (double) volumeUSD{
    return [self _castDouble:@"24h_volume_usd"];
}
- (double) marketCapUSD{
    return [self _castDouble:@"market_cap_usd"];
}
- (double) availableSupply {
    return [self _castDouble:@"available_supply"];
}
- (double) totalSupply {
    return [self _castDouble:@"total_supply"];
}
- (double) maxSupply {
    return [self _castDouble:@"max_supply"];
}

- (CGFloat) _castFloat:(NSString*)key {
    return [[internal objectForKey:key] floatValue];
}
- (CGFloat) percent_hourly {
    return [self _castFloat:@"percent_change_1h"];
}
- (CGFloat) percent_daily {
    return [self _castFloat:@"percent_change_24h"];
}
- (CGFloat) percent_weekly{
    return [self _castFloat:@"percent_change_7d"];
}

@end
