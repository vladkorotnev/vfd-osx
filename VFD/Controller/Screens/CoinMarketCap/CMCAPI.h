//
//  CMCAPI.h
//  VFD
//
//  Created by Akasaka Ryuunosuke on 09/12/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMCTicker.h"

/**
 @typedef CMCIdentifier
 @brief A crypto currency identifier according to the CoinMarketCap API (e.g. "bitcoin")
 @seealso CMCAPI
 @seealso CMCTicker
 */
typedef NSString CMCIdentifier;

/**
 @class CMCAPI
 @brief CoinMarketCap API connector
 @seealso CMCTicker
 */
@interface CMCAPI : NSObject
/**
 @method sharedInstance
 @brief Returns the singleton instance of the CoinMarketCap API connector
 */
+ (CMCAPI*) sharedInstance;
/**
 @method tickerForIdentifier:
 @brief Retrieves the current ticker information for the specified crypto currency
 */
- (CMCTicker*) tickerForIdentifier: (CMCIdentifier *)identifier;
@end
