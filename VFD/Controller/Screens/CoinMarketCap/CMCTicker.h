//
//  CMCTicker.h
//  VFD
//
//  Created by Akasaka Ryuunosuke on 09/12/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class CMCTicker
 @brief A ticker for a crypto currency, retrieved from CoinMarketCap
 @seealso CMCAPI
 */
@interface CMCTicker : NSObject
/**
 @method tickerFromJSONObject:
 @brief Unwraps a JSON object from the CoinMarketCap API into a CMCTicker object
 @seealso tickerForIdentifier:
 */
+ (CMCTicker*) tickerFromJSONObject:(NSDictionary*) object;
/**
 @method tickerForIdentifier:
 @brief Returns a ticker from the CoinMarketCap API for a specified crypto currency. Equivalent to the method of the CMCAPI object.
 @seealso CMCAPI
 @seealso tickerFromJSONObject:
 */
+ (CMCTicker*) tickerForIdentifier: (NSString *)identifier;

/**
 @property identifier
 @brief The ticker's crypto currency identifier
 */
- (NSString*) identifier;
/**
 @property name
 @brief The ticker's crypto currency user-readable name
 */
- (NSString*) name;
/**
 @property description
 @brief The ticker object description for debug purposes. For user-readable name, see @see name.
 @seealso name
 */
- (NSString*) description;
/**
 @property symbol
 @brief The ticker's crypto currency symbol.
 */
- (NSString*) symbol;
/**
 @property rank
 @brief The ticker's crypto currency rank in the CoinMarketCap rating.
 */
- (NSUInteger) rank;
/**
 @property priceUSD
 @brief The price of the ticker's crypto currency in USD (per unit).
 @seealso priceBTC
 */
- (double) priceUSD;
/**
 @property priceBTC
 @brief The price of the ticker's crypto currency in BTC (per unit).
 @seealso priceUSD
 */
- (double) priceBTC;
/**
 @property volumeUSD
 @brief The ticker's crypto currency market volume in USD.
 */
- (double) volumeUSD;
/**
 @property marketCapUSD
 @brief The ticker's crypto currency market capitalization in USD.
 */
- (double) marketCapUSD;
/**
 @property availableSupply
 @brief The ticker's crypto currency available supply.
 @seealso totalSupply
 @seealso maxSupply
 */
- (double) availableSupply;
/**
 @property totalSupply
 @brief The ticker's crypto currency total supply.
 @seealso availableSupply
 @seealso maxSupply
 */
- (double) totalSupply;
/**
 @property maxSupply
 @brief The ticker's crypto currency maximum possible supply.
 @seealso totalSupply
 @seealso availableSupply
 */
- (double) maxSupply;
/**
 @property percent_hourly
 @brief The ticker's crypto currency price change within the last hour, in percent
 @seealso percent_daily
 @seealso percent_weekly
 */
- (CGFloat) percent_hourly;
/**
 @property percent_daily
 @brief The ticker's crypto currency price change within the last 24 hours, in percent
 @seealso percent_hourly
 @seealso percent_weekly
 */
- (CGFloat) percent_daily;
/**
 @property percent_weekly
 @brief The ticker's crypto currency price change within the last 7 days, in percent
 @seealso percent_daily
 @seealso percent_hourly
 */
- (CGFloat) percent_weekly;

@end
