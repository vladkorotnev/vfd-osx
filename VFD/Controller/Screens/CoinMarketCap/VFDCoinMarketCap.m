//
//  VFDCoinMarketCap.m
//  VFD
//
//  Created by Akasaka Ryuunosuke on 09/12/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDCoinMarketCap.h"

typedef enum : NSUInteger {
    DisplayStateHourly,
    DisplayStateDaily,
    DisplayStateWeekly,
    DisplayStateBalance,
    DisplayStateBalanceUSD,
    DisplayStateDelta
} TickerDisplayState;

@implementation VFDCoinMarketCap {
    CMCTicker * ticker;
    TickerDisplayState tds;
}
- (VFDCoinMarketCap *) initWithIdentifier: (CMCIdentifier *) identifier {
    self = [super init];
    if(self)
    {
        self.identifier = identifier;
    }
    return self;
}
- (NSTimeInterval) refreshInterval {
    return 4.0;
}
- (NSTimeInterval) displayTime {
    return [self isInvestor] ? (self.refreshInterval * 6) : (self.refreshInterval * 3);
}
- (NSTimeInterval) overrideCarouselDisplayTiming {
    return [self displayTime];
}
- (bool) isModal {
    return true;
}

- (bool) hasUpdates {
    return true;
}
- (bool) isInvestor {
    return (self.balance > 0 && self.investedUSD > 0);
}
- (void) drawOnVFD:(NSObject<VFDDevice> *)device {
    [device clear];
    [device setTopLine:@"Loading data..." aligned:VFDLineCentered];
    [device setBottomLine:self.identifier aligned:VFDLineCentered];
    
    ticker = [CMCTicker tickerForIdentifier:self.identifier];
    if (ticker) {
        tds = 0;
            NSString * top = [NSString stringWithFormat:@"%@: $%.02f", ticker.symbol, ticker.priceUSD];
            NSString * bottom = [NSString stringWithFormat:@"1h: %.02f%%", ticker.percent_hourly];
            [device setTopLine:top aligned:VFDLineNone];
            [device setBottomLine:bottom aligned:VFDLineRight];
    } else {
        [device setTopLine:@"Loading data failed" aligned:VFDLineCentered];
    }
}

- (CGFloat) currentBalanceInUSD {
    return self.balance * ticker.priceUSD;
}
- (CGFloat) currentDeltaInUSD {
    return [self currentBalanceInUSD] - self.investedUSD;
}

- (void) updateOnVFD:(NSObject<VFDDevice> *)device {
    NSString * line;
    if (ticker) {
        if ([self isInvestor]) {
            // investor delta monitor mode
            if (tds == DisplayStateDelta) tds = 0;
            else tds++;
        } else {
            // ticker view mode
            if (tds == DisplayStateWeekly) tds = 0;
            else tds++;
        }
        switch (tds) {
            case DisplayStateHourly:
                line = [NSString stringWithFormat:@"1h: %.02f%%", ticker.percent_hourly];
                break;
            case DisplayStateDaily:
                line = [NSString stringWithFormat:@"24h: %.02f%%", ticker.percent_daily];
                break;
                
            case DisplayStateWeekly:
                line = [NSString stringWithFormat:@"7d: %.02f%%", ticker.percent_weekly];
                break;
                
            case DisplayStateBalance:
                line = [NSString stringWithFormat:@"%.06f %@", self.balance, ticker.symbol];
                break;
                
            case DisplayStateBalanceUSD:
                line = [NSString stringWithFormat:@"($%.02f)", [self currentBalanceInUSD]];
                break;
                
            case DisplayStateDelta:
                line = [NSString stringWithFormat:@"gross: $ %.02f", [self currentDeltaInUSD]];
                break;
                
                
            default:
                break;
        }
        [device setBottomLine:line aligned:VFDLineRight];
    }
}
@end
