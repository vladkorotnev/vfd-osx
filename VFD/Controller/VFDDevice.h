//
//  VFDDevice.h
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VFDString;

/**
 *  @typedef VFDLineAlign
 *  @brief Describes methods of aligning lines on screen
 */
typedef enum : NSUInteger {
    /** Don't care */
    VFDLineNone = 0,
    /** Center the line on screen if it fits */
    VFDLineCentered = 1,
    /** Scroll the line if the hardware supports scrolling */
    VFDLineScrolled = 2,
    /** Right-align the line */
    VFDLineRight = 4,
    /** If the line fits, center it, otherwise scroll, if hardware supports it */
    VFDLineCenteredOrScrolled = (VFDLineCentered | VFDLineScrolled)
} VFDLineAlign;


/**
 * @protocol VFDDevice
 * @brief A protocol for accessing VFD/LCD character display device drivers
 */
@protocol VFDDevice <NSObject>

/**
 * @method width
 * @brief Returns the width (in characters) of the current display device
 */
- (NSUInteger) width;
/**
 * @method height
 * @brief Returns the height (in lines) of the current display device
 */
- (NSUInteger) height;

/**
 * @method overwrite
 * @brief Brings the device into terminal mode with overwrite, for further drawing via sendString:
 */
- (void) overwrite;
/**
 * @method sendString
 * @brief Sends a raw command/data line to the device driver
 * @param string the data to be sent
 */
- (void) sendString:(NSString*)string;
/**
 * @method clear
 * @brief Clears the current device display
 */
- (void) clear;

/**
 * @method setTopLine
 * @brief Sets the top line of the display device
 * @param line the line to draw
 */
- (void) setTopLine: (VFDString*)line;
/**
 * @method setBottomLine
 * @brief Sets the bottom line of the display device
 * @param line the line to draw
 */
- (void) setBottomLine: (VFDString*)line;

/**
 * @method setTopLine:aligned
 * @brief Sets the top line of the display device to a specified string with the specified alignment method
 * @param line the content of the line to display
 * @param align the alignment method to use
 */
- (void) setTopLine: (NSString*)line aligned:(VFDLineAlign) align;

/**
 * @method setBottomLine:aligned
 * @brief Sets the bottom line of the display device to a specified string with the specified alignment method
 * @param line the content of the line to display
 * @param align the alignment method to use
 */
- (void) setBottomLine: (NSString*)line aligned:(VFDLineAlign) align;

/**
 * @method setCursorRow:Col
 * @brief Sets the coordinate on the screen for further writing via sendString
 * @param row the line of the display
 * @param col the column of the display
 * @see sendString
 */
- (void) setCursorRow: (NSUInteger)row Col:(NSUInteger)col;
@end


