//
//  VFDScreenManager.m
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDScreenManager.h"


@implementation VFDScreenManager
{
    NSMutableArray *screenStack;
    VFDScreen *currentScreen;
}

+ (VFDScreenManager *)sharedInstance
{
    static VFDScreenManager *sharedInstance = nil;
    if (sharedInstance == nil)
    {
        sharedInstance = [[self alloc] init];
    }
    return sharedInstance;
}

- (VFDScreenManager *) init {
    self  = [super init];
    if (self) {
        screenStack = [NSMutableArray new];
        currentScreen = [VFDBlankScreen new];
    }
    return self;
}

- (void) popScreen {
    if (screenStack.count > 0) {
        currentScreen = [screenStack lastObject];
        [screenStack removeLastObject];
        [currentScreen drawOnVFD:self.device];
    } else {
        currentScreen = nil;
        [self.device clear];
    }
}

- (void) pullScreen: (VFDScreen*) screen {
    [self _stopRefreshingScreen:screen];
    [self _cancelExpirationOfScreen:screen];
    if (screen == currentScreen) {
        [self popScreen];
    } else if ([screenStack containsObject:screen]) {
        [screenStack removeObject:screen];
    }
}


- (void) redrawIfNeeded {
    if ([currentScreen hasUpdates]) {
        [self redraw];
    }
}

- (void) redraw {
    if (currentScreen) {
        [currentScreen drawOnVFD:self.device];
        [self _queueRefreshOfScreen:currentScreen];
    }
}

- (void) updateIfNeeded {
    if ([currentScreen hasUpdates]) {
        [self update];
    }
}

- (void) update {
    if (currentScreen) {
        [currentScreen updateOnVFD:self.device];
        [self _queueRefreshOfScreen:currentScreen];
    }
}


- (void) slipScreenBelowCurrent: (VFDScreen *) screen {
    if (currentScreen == screen || [screenStack containsObject:screen]) {
        if ([self isCurrentScreen:screen]) {
            [self redrawIfNeeded];
        }
    }
    if (!currentScreen) {
        currentScreen = screen;
        [self redraw];
    } else {
        [screenStack addObject:screen];
    }
    [self _queueExpirationOfScreenIfNeeded:screen];
    [self _queueRefreshOfScreen:screen];
}


- (void) pushScreen: (VFDScreen*) screen {
    if (currentScreen == screen || [screenStack containsObject:screen]) {
        [self _queueExpirationOfScreenIfNeeded:screen];
        if (currentScreen == screen) {
            [screen drawOnVFD:self.device];
        }
        return;
    }
   if(currentScreen) [screenStack addObject:currentScreen];
    currentScreen = screen;
    [self _queueExpirationOfScreenIfNeeded:screen];
    [self _queueRefreshOfScreen:screen];
    [currentScreen drawOnVFD:self.device];
}

- (bool) isCurrentScreen: (VFDScreen*) screen {
    return currentScreen == screen;
}

#pragma mark - Internal Timing
/// Dequeue all expiration events for a screen
- (void) _cancelExpirationOfScreen: (VFDScreen *) screen {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_displayTimeExpiredForScreen:) object:screen];
}

/// Queue an expiration event for a screen
- (void) _queueExpirationOfScreenIfNeeded: (VFDScreen *)screen {
    if (!screen.isModal) {
        [self _cancelExpirationOfScreen:screen];
        [self performSelector:@selector(_displayTimeExpiredForScreen:) withObject:screen afterDelay:screen.displayTime];
    }
}

/// Called when the screen expires
- (void) _displayTimeExpiredForScreen: (VFDScreen*)screen {
    [self pullScreen:screen];
}

/// Enqueue next refresh for a screen
- (void) _queueRefreshOfScreen:(VFDScreen *)screen {
    [self _stopRefreshingScreen:screen];
    if (screen.refreshInterval > 0) {
        [self performSelector:@selector(_refreshTimeForScreen:) withObject:screen afterDelay:screen.refreshInterval];
    }
}

/// Dequeue all refreshes for a screen
- (void) _stopRefreshingScreen:(VFDScreen*)screen {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_refreshTimeForScreen:) object:screen];
}

/// Called when it's time to refresh a screen
- (void) _refreshTimeForScreen:(VFDScreen*)screen {
    if (screen == currentScreen) {
        [self update];
    }
    [self _queueRefreshOfScreen:screen];
}

@end
