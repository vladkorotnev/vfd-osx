//
//  VFDString.m
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDString.h"
#import "NSStringCentering.h"
@implementation VFDString
- (VFDString *) initWithString:(NSString *) string alignment:(VFDLineAlign)alignment {
    self = [super init];
    if (self) {
        self.content = string;
        self.alignment = alignment;
    }
    return self;
}

- (VFDString *) initWithString:(NSString *) string {
    return [self initWithString:string alignment:VFDLineNone];
}

+ (VFDString *) simpleStringWithString:(NSString *)string {
    return [VFDString string:string aligned:VFDLineNone];
}

+ (VFDString *) centeredStringWithString:(NSString *)string {
    return [VFDString string:string aligned:VFDLineCenteredOrScrolled];
}
+ (VFDString *) string: (NSString*)string aligned:(VFDLineAlign) alignment {
    return [[VFDString alloc]initWithString:string alignment:alignment];
}

- (NSUInteger) leftOffsetForDevice: (NSObject<VFDDevice>*)device {
    if (self.alignment == VFDLineCentered || self.alignment == VFDLineCenteredOrScrolled) {
        return [self.content leftPaddingForCenteringStringInWidth:device.width];
    } else if (self.alignment == VFDLineRight) {
        return [self.content leftPaddingForRightAlignStringInWidth:device.width];
    }
    return 0;
}
@end
