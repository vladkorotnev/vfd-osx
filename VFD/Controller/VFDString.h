//
//  VFDString.h
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VFDDevice.h"


/**
 * @class VFDString
 * @brief Describes a string with it's aligmnent method for further display
 */
@interface VFDString : NSObject
/** 
 * @property alignment
 * @brief The way to align the string on the screen
 * @see VFDLineAlign
 */
@property (nonatomic) VFDLineAlign alignment;
/**
 * @property content
 * @brief The content of the line
 */
@property (nonatomic,retain) NSString* content;
/**
 * @method initWithString:alignment
 * @brief Create an instance of VFDString with specified content and alignment
 * @param string The content of the VFDString
 * @param alignment The formatting of the VFDString
 * @see initWithString
 */
- (VFDString *) initWithString:(NSString *) string alignment: (VFDLineAlign) alignment;
/**
 * @method initWithString
 * @brief Create an instance of VFDString with specified content
 * @param string The content of the VFDString
 * @see initWithString:alignment
 */
- (VFDString *) initWithString:(NSString *) string;
/**
 * @method string:aligned:
 * @brief Create an instance of VFDString with specified content and alignment
 * @param string Content of the VFDString
 * @param alignment Formatting of the VFDString
 * @see simpleStringWithString
 * @see centeredStringWithString
 */
+ (VFDString *) string: (NSString*)string aligned:(VFDLineAlign) alignment;
/**
 * @method simpleStringWithString
 * @brief Create an unformatted VFDString
 * @param string The content of the VFDString
 * @see string:aligned:
 * @see centeredStringWithString
 */
+ (VFDString *) simpleStringWithString:(NSString *)string;
/**
 * @method simpleStringWithString
 * @brief Create a centered VFDString
 * @param string The content of the VFDString
 * @see string:aligned:
 * @see simpleStringWithString
 */
+ (VFDString *) centeredStringWithString:(NSString *)string;
/**
 * @method leftOffsetForDevice
 * @brief The starting column to position the cursor at, when drawing on the specified device
 * @param device The device to calculate against
 */
- (NSUInteger) leftOffsetForDevice: (NSObject<VFDDevice>*)device;
@end
