//
//  CD7220.m
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "CD7220.h"




@implementation CD7220
- (CD7220*) initWithPort:(ORSSerialPort*) port {
    self = [super init];
    if (self) {
        self->port = port;
        if(![self->port isOpen])
            [self->port open];
        [self reset];
        
        [self clear];
    }
    return self;
}
- (NSUInteger) width {
    return 20;
}
- (NSUInteger) height {
    return 2;
}
- (void) sendString:(NSString*)string {
  //  NSLog(@"%@",string);
#define NSDOSRussianStringEncoding 0x8000041B
    [self->port sendData:[string dataUsingEncoding:NSDOSRussianStringEncoding]];
}

- (void) reset {
    
    [self sendString:@"\x1b\x40"];
}
- (void) verticalScroll {
    [self sendString:@"\x1b\x12"];
}
- (void) horizontalScroll {
    [self sendString:@"\x1b\x13"];
}
- (void) overwrite {
    [self sendString:@"\x1b\x11"];
}
- (void) clear {
    [self sendString:@"\x0c"];
}

- (void) setTopLine: (NSString*)line aligned:(VFDLineAlign) align {
    
    switch (align) {
        case VFDLineNone:
            [self _setTopLine:line];
            break;
        case VFDLineCentered:
            if (line.length == [self width]) {
                [self _setTopLine:line];
            } else {
                [self _setTopLine:[line stringCenteredInWidth:[self width]]];
            }
            break;
        case VFDLineRight:
            if (line.length == [self width]) {
                [self _setTopLine:line];
            } else {
                [self _setTopLine:[line stringRightAlignedInWidth:[self width]]];
            }
            break;
        case VFDLineScrolled:
            [self _scrollTopLine:[line stringByAppendingString:@"      "]];
            break;
        case VFDLineCenteredOrScrolled:
            if(line.length > [self width]) {
                [self _scrollTopLine:[line stringByAppendingString:@"      "]];
            } else if (line.length == [self width]) {
                [self _setTopLine:line];
            } else {
                [self _setTopLine:[line stringCenteredInWidth:[self width]]];
            }
            break;
        default:
            [self _setTopLine:line];
            break;
    }
}
- (void) setBottomLine: (NSString*)line aligned:(VFDLineAlign) align {
    switch (align) {
        case VFDLineNone:
            [self _setBottomLine:line];
            break;
        case VFDLineCentered:
        case VFDLineScrolled:
        case VFDLineCenteredOrScrolled:
            if (line.length == [self width]) {
                [self _setBottomLine:line];
            } else {
                [self _setBottomLine:[line stringCenteredInWidth:[self width]]];
            }
            break;
        case VFDLineRight:
            if (line.length == [self width]) {
                [self _setBottomLine:line];
            } else {
                [self _setBottomLine:[line stringRightAlignedInWidth:[self width]]];
            }
            break;
        default:
            [self _setBottomLine:line];
            break;
    }
}

- (void) _setTopLine: (NSString*)line {
    [self sendString:[NSString stringWithFormat:@"\x1b\x51\x41%@\x0D",line]];
}
- (void) _setBottomLine:(NSString*)line {
    [self sendString:[NSString stringWithFormat:@"\x1b\x51\x42%@\x0D",line]];
}

- (void) setTopLine: (VFDString*)line {
    [self setTopLine:line.content aligned:line.alignment];
}
- (void) setBottomLine: (VFDString*)line {
    [self setBottomLine:line.content aligned:line.alignment];
}


- (void) _scrollTopLine: (NSString*)line {
    [self sendString:[NSString stringWithFormat:@"\x1b\x51\x44 %@\x0D",line]];
}


- (void) setCursorPositionLeft:(uint8_t)col andTop:(uint8_t)row {
    [self sendString:[NSString stringWithFormat:@"\x1b\x6c%c%c",row,col]]; //NB: CD7220 datasheet has the row and col arg order mixed up!
}
- (void) setCursorRow: (NSUInteger)row Col:(NSUInteger)col {
    [self setCursorPositionLeft:(uint8_t) row andTop:(uint8_t) col];
}
@end
