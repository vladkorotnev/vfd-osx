//
//  CD7220.h
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ORSSerialPort.h"
#import "NSStringCentering.h"
#import "VFDDevice.h"
#import "VFDString.h"

/**
 @class CD7220
 @brief The CD7220 chipset driver
 @discussion   Provides access to CD7220 powered display devices
 */
@interface CD7220 : NSObject <VFDDevice>
{
    ORSSerialPort *port;
}

/**
 * @method initWithPort
 * @brief Instantiates the CD7220 driver at the specified serial port
 * @param port The port to use (please configure beforehand with required parity and baud rate)
 */
- (CD7220*) initWithPort:(ORSSerialPort*) port;
/**
 * @method reset
 * @brief Resets the CD7220 device
 */
- (void) reset;
/**
 * @method verticalScroll
 * @brief Sets the device to raw terminal mode, vertically scrolling if contents won't fit
 */
- (void) verticalScroll;
/**
 * @method horizontalScroll
 * @brief Sets the device to raw terminal mode, horizontally scrolling if contents won't fit
 */
- (void) horizontalScroll;


@end
