//
//  VFDScreenManager.h
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VFDDevice.h"
#import "VFDScreen.h"
#import "VFDBlankScreen.h"

/**
 @class VFDScreenManager
 @brief Manages the VFD Screens with a display stack
 @discussion Allows to easily manage the screens on a device by using a display stack. Automatically keeps track of screen refreshing, expiration and redrawing.
 @see VFDScreen
 */
@interface VFDScreenManager : NSObject
/**
 * @method sharedInstance
 * @brief Return the active VFDScreenManager instance
 */
+ (VFDScreenManager*) sharedInstance;
/**
 * @method pushScreen
 * @brief Adds a new screen to the top of the screen stack and draws it
 * @param screen The screen to add to the stack
 */
- (void) pushScreen: (VFDScreen*) screen;
/**
 * @method slipScreenBelowCurrent
 * @brief Adds a new screen below the currently displayed in the screen stack
 * @discussion Insert a new screen below the currently displayed. Once the current screen expires, the inserted will be displayed.
 * @param screen The screen to insert below the current screen
 */
- (void) slipScreenBelowCurrent: (VFDScreen *) screen;
/**
 * @method pullScreen
 * @brief Remove a screen from the display stack
 * @param screen The screen to remove
 */
- (void) pullScreen: (VFDScreen*) screen;
/**
 * @method popScreen
 * @brief Remove the currently displayed screen from the display stack
 */
- (void) popScreen;
/**
 * @method isCurrentScreen
 * @brief Checks whether the screen is the currently active screen
 * @param screen The screen to check
 * @return Is the screen currently displayed
 */
- (bool) isCurrentScreen: (VFDScreen*) screen;
/**
 * @method redrawIfNeeded
 * @brief If the currently displayed screen has some updates to display, perform a full redraw of the device
 * @see redraw
 * @see updateIfNeeded
 * @see update
 */
- (void) redrawIfNeeded;
/**
 * @method redraw
 * @brief Perform a full redraw of the device
 * @see redrawIfNeeded
 * @see updateIfNeeded
 * @see update
 */
- (void) redraw;
/**
 * @method updateIfNeeded
 * @brief If the currently displayed screen has some updates to display, perform a partial redraw of the device
 * @see redrawIfNeeded
 * @see redraw
 * @see update
 */
- (void) updateIfNeeded;
/**
 * @method update
 * @brief Perform a partial redraw of the device
 * @see redrawIfNeeded
 * @see redraw
 * @see updateIfNeeded
 */
- (void) update;
/**
 * @property device
    @brief The device managed by the current screen manager
 */
@property (nonatomic, retain) NSObject<VFDDevice> *device;

@end
