//
//  VFDScreen.m
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import "VFDScreen.h"

@implementation VFDScreen

- (VFDScreen *) initWithTopLine:(VFDString*)topLine bottomLine:(VFDString*)bottomLine displayTime:(NSTimeInterval)displayTime modal:(bool)modal {
    self = [super init];
    if (self) {
        self.topLine = topLine;
        self.bottomLine = bottomLine;
        self.displayTime = displayTime;
        self.isModal = modal;
    }
    return self;
}

- (void) drawOnVFD: (NSObject<VFDDevice>*)device {
    if (self.shouldClearPriorToDrawing) {
        [device clear];
    }
    // bottom draws first because of bug in CD7220 display driver not able to hardware scroll properly
    if (self.bottomLine) {
        [device setBottomLine:self.bottomLine.content aligned:self.bottomLine.alignment];
    }
    if (self.topLine) {
        [device setTopLine:self.topLine.content aligned:self.topLine.alignment];
    }
}
- (void) updateOnVFD: (NSObject<VFDDevice>*)device {
    [self drawOnVFD:device]; // developer implements their own less blinky update should they need to.
}
- (bool) hasUpdates {
    return false;
}


@end
