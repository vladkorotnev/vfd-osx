//
//  VFDScreen.h
//  VFD
//
//  Created by Akasaka Ryuunosuke on 18/06/17.
//  Copyright (c) 2017 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VFDString.h"
#import "VFDDevice.h"
/**
 @class VFDScreen
 @brief An object describing a display state (text, timing, etc.)
 */
@interface VFDScreen : NSObject
/**
 @property topLine
 @brief The contents of the top line of the screen
 */
@property (nonatomic,retain) VFDString *topLine;
/**
 @property bottomLine
 @brief The contents of the bottom line of the screen
 */
@property (nonatomic,retain) VFDString *bottomLine;
/**
 @property shouldClearPriorToDrawing
 @brief Specifies whether the display should be cleared prior to drawing this screen
 */
@property (nonatomic) bool shouldClearPriorToDrawing;
/**
 @property isModal
 @brief Specifies whether the screen does not hide after it's displayTime
 @see displayTime
 */
@property (nonatomic) bool isModal;
/**
 @property isInactive
 @brief Specifies whether the screen should be skipped during drawing of a parent screen
 */
@property (nonatomic) bool isInactive;
/**
 @property displayTime
 @brief Specifies the time after which the screen should be removed from the display stack
 */
@property (nonatomic) NSTimeInterval displayTime;
/**
 @property refreshInterval
 @brief Specifies the interval for refreshing the screen while it's on a device
 */
@property (nonatomic) NSTimeInterval refreshInterval;
/**
 * @method drawOnVFD
 * @brief Performs initial drawing of the screen on the specified device
 * @param device The device to draw the screen on
 */
- (void) drawOnVFD: (NSObject<VFDDevice>*)device;
/**
 * @method updateOnVFD
 * @brief Performs update of the screen on the specified device
 * @param device The device to draw the screen on
 * @see refreshInterval
 */
- (void) updateOnVFD: (NSObject<VFDDevice>*)device;
/**
 * @method hasUpdates
 * @brief Returns whether the screen needs to be redrawn on the next refresh interval
 */
- (bool) hasUpdates;
/**
 * @method initWithTopLine:bottomLine:displayTime:modal
 * @brief Instantiates a screen object
 * @param topLine The line to display as the top line
 * @param bottomLine The line to display as the bottom line
 * @param displayTime The time to clear the screen from the display stack after
 * @param modal Whether the screen should be removed after displayTime
 */
- (VFDScreen *) initWithTopLine:(VFDString*)topLine bottomLine:(VFDString*)bottomLine displayTime:(NSTimeInterval)displayTime modal:(bool)modal;
@end
